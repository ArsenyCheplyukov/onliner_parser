from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from tqdm import tqdm
from time import sleep

# Set up Selenium webdriver
driver = webdriver.Chrome()  # or choose the appropriate webdriver for your browser
driver.maximize_window()  # Maximizes the browser window
url = "https://catalog.onliner.by/"
driver.get(url)

# Find all categories
elements_class = "catalog-navigation-classifier__item "
elements = driver.find_elements(By.CSS_SELECTOR, "li." + elements_class)

out_data = set()  # Changed to a set

# Iterate over elements and handle exceptions
for i, element in tqdm(enumerate(elements)):
    try:
        element.click()
    except (NoSuchElementException, ElementNotInteractableException, TimeoutException):
        continue  # Skip to the next element if it cannot be clicked

    # Click on the sub-element
    staff_filtering = "div.catalog-navigation-list__aside-list"
    list_element = driver.find_elements(By.CSS_SELECTOR, staff_filtering)[i]
    sub_elements_class = 'div.catalog-navigation-list__dropdown-list'
    sub_elements = list_element.find_elements(By.CSS_SELECTOR, sub_elements_class)

    for sub_element in tqdm(sub_elements, leave=False):
        try:
            # Find all href elements within the category_element
            href_elements = sub_element.find_elements(By.TAG_NAME, "a")

            # Extract the href attribute from each element
            hrefs = [element_.get_attribute("href") for element_ in href_elements]
            # Add links to final set
            out_data.update(hrefs)  # Changed to update the set
        except (NoSuchElementException, ElementNotInteractableException, TimeoutException):
            continue  # Skip to the next sub-element if the category element cannot be found

with open('group_links.txt', 'w', encoding='utf8') as f:
    f.write('\n'.join(out_data))  # Write the set as a newline-separated string to the file

# Close the browser
driver.quit()