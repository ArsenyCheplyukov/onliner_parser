import pickle
import pandas as pd

file_names = [
    f'serialized_data_{i+1}_{j*100}.pickle'
    for i, j in enumerate([4, 14, 14, 18, 20, 18, 14, 13, 12, 18, 18, 18, 16, 17, 9])
] + ['serialized_data_700.pickle']

data = []
for file_name in file_names:
    with open(file_name, 'rb') as file:
        file_data = pickle.load(file)
        data.extend(file_data)

df = pd.DataFrame(data, columns=['Категория', 'Товар', 'Заголовок отзыва',
                                 'Комментарий к оценке', 'Достоинства',
                                 'Недостатки', 'Отзыв'])

output_file = 'onliner_review_dataset_extended.csv'
df.to_csv(output_file, index=False, encoding='utf8')
