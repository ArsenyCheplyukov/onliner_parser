from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, \
                                       ElementNotInteractableException, \
                                       StaleElementReferenceException, \
                                       ElementClickInterceptedException, \
                                       WebDriverException, \
                                       TimeoutException
from selenium.webdriver.common.by import By
from tqdm import tqdm
from time import sleep
from random import uniform

# Set up Selenium webdriver
driver = webdriver.Chrome()  # or choose the appropriate webdriver for your browser
driver.maximize_window()  # Maximizes the browser window
# driver.get(url)

# Read data from out.txt to get links
with open("group_links.txt", "r", encoding="utf8") as f:
    a = f.readlines()

out_data = set()

with open('custom_links.txt', 'w', encoding='utf8') as f:
    # Itterate through all links in doc
    for i, link in tqdm(enumerate(a), leave=False):
        # delete all new lines
        if link.endswith("\n"):
            link = link[:-1]

        try:
            # open link
            driver.get(link)

            exit_state = 1
            while exit_state:
                # itterate through all customs
                elements_class = "div.schema-product__group"
                elements = driver.find_elements(By.CSS_SELECTOR, elements_class)

                # Find all href elements within the category_element
                href_elements = []
                for element in elements:
                    try:
                        review_count_element = element.find_element(By.CLASS_NAME, "schema-product__review-count")
                        if review_count_element:
                            href_element = element.find_element(By.TAG_NAME, "a")
                            href_elements.append(href_element.get_attribute("href")+"/reviews")
                    except (NoSuchElementException, StaleElementReferenceException,  TypeError):
                        pass
                # Extract the href attribute from each element
                out_data.update(href_elements)  # Changed to update the set

                # itterate over all links to customs
                try:
                    elem = driver.find_element(By.CSS_SELECTOR, "div.schema-pagination.schema-pagination_visible")

                    # Get the class names of the element
                    elem.click()
                    sleep(uniform(0.1, 1.5))

                    # Check if any class name contains the desired word
                    if elem.find_elements(By.CSS_SELECTOR, "a.schema-pagination__main.schema-pagination__main_disabled"):
                        exit_state = 0
                except (NoSuchElementException, ElementNotInteractableException, ElementClickInterceptedException, StaleElementReferenceException):
                    exit_state = 0
        except (WebDriverException, TimeoutException):
            pass

        if i % 100 == 99 and i:
            f.write('\n'.join(out_data))  # Write the set as a newline-separated string to the file
            print("Saving on:", link, "link; With link number:", i)
            out_data = set()


# Close the browser
driver.quit()

# from selenium import webdriver
# from selenium.common.exceptions import NoSuchElementException, \
#                                        ElementNotInteractableException, \
#                                        StaleElementReferenceException, \
#                                        ElementClickInterceptedException, \
#                                        WebDriverException
# from selenium.webdriver.common.by import By
# from tqdm import tqdm
# from time import sleep
# from random import uniform
# import concurrent.futures

# # Set up Selenium webdriver
# driver = webdriver.Chrome()  # or choose the appropriate webdriver for your browser
# driver.maximize_window()  # Maximizes the browser window
# # driver.get(url)

# # Read data from out.txt to get links
# with open("group_links.txt", "r", encoding="utf8") as f:
#     a = f.readlines()

# out_data = set()

# def process_link(link):
#     try:
#         # open link
#         driver.get(link)

#         exit_state = 1
#         while exit_state:
#             # itterate through all customs
#             elements_class = "div.schema-product__group"
#             elements = driver.find_elements(By.CSS_SELECTOR, elements_class)

#             # Find all href elements within the category_element
#             href_elements = []
#             for element in elements:
#                 try:
#                     review_count_element = element.find_element(By.CLASS_NAME, "schema-product__review-count")
#                     if review_count_element:
#                         href_element = element.find_element(By.TAG_NAME, "a")
#                         href_elements.append(href_element.get_attribute("href")+"/reviews")
#                 except (NoSuchElementException, StaleElementReferenceException):
#                     pass
#             # Extract the href attribute from each element
#             out_data.update(href_elements)  # Changed to update the set

#             # itterate over all links to customs
#             try:
#                 elem = driver.find_element(By.CSS_SELECTOR, "div.schema-pagination.schema-pagination_visible")

#                 # Get the class names of the element
#                 elem.click()
#                 sleep(uniform(0.3, 2))

#                 # Check if any class name contains the desired word
#                 if elem.find_elements(By.CSS_SELECTOR, "a.schema-pagination__main.schema-pagination__main_disabled"):
#                     exit_state = 0
#             except (NoSuchElementException, ElementNotInteractableException, ElementClickInterceptedException, StaleElementReferenceException):
#                 exit_state = 0
#     except WebDriverException:
#         driver.refresh()

# with concurrent.futures.ThreadPoolExecutor() as executor:
#     # Submit tasks to the executor for each link
#     futures = [executor.submit(process_link, link.strip()) for link in a]

#     # Wait for all tasks to complete
#     for future in tqdm(concurrent.futures.as_completed(futures), total=len(futures)):
#         # Handle any exceptions that occurred during execution
#         try:
#             future.result()
#         except Exception as e:
#             print(f"An error occurred: {str(e)}")

# # Write the set as a newline-separated string to the file
# with open('custom_links.txt', 'w', encoding='utf8') as f:
#     f.write('\n'.join(out_data))

# # Close the browser
# driver.quit()