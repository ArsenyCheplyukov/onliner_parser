# import asyncio
# from selenium import webdriver
# from selenium.webdriver.chrome.service import Service
# from selenium.webdriver.common.by import By
# from selenium.webdriver.chrome.options import Options
# import pandas as pd
# from tqdm import tqdm
# import warnings
# import logging

# # Disable logging messages from the specified logger
# logging.getLogger("websockets").setLevel(logging.ERROR)

# warnings.filterwarnings("ignore")

# LINKS_FILE = "custom_links.txt"
# CONCURRENT_REQUESTS = 100  # Number of concurrent requests to send
# EXCEL_FILE = "onliner_review_dataset_test_new.xlsx"


# async def process_link(link):
#     # Set up the Chrome WebDriver
#     chrome_options = Options()
#     chrome_options.add_argument('--headless')  # Run Chrome in headless mode (without opening a browser window)
#     service = Service()
#     driver = webdriver.Chrome(service=service, options=chrome_options)

#     try:
#         # Load the page with JavaScript rendering
#         driver.get(link)

#         # Find the desired element by its CSS selector
#         elements = driver.find_elements(By.CSS_SELECTOR, "div#list.offers-reviews__list")

#         # Extract the desired information
#         category = driver.find_elements(By.CSS_SELECTOR, 'li.breadcrumbs__item')[-2].text
#         customs = driver.find_element(By.CSS_SELECTOR, 'h1.catalog-masthead__title.js-nav-header').text

#         ans = []
#         for element in elements:
#             title_review = element.find_element(By.CSS_SELECTOR, "span.offers-reviews__description.offers-reviews__description_alter.offers-reviews__description_middle.offers-reviews__description_font-weight_bold.offers-reviews__description_condensed").text
#             mark_comment = element.find_element(By.CSS_SELECTOR, "offers-reviews__grade-sign").text
#             pros = element.find_element(By.CSS_SELECTOR, "div.offers-reviews__description.offers-reviews__description_alter.offers-reviews__description_base.offers-reviews__description_condensed-alter.offers-reviews__description_max-width_xxxxll").text
#             cons = element.find_element(By.CSS_SELECTOR, "div.offers-reviews__description.offers-reviews__description_alter.offers-reviews__description_base.offers-reviews__description_max-width_xxxxll").text
#             review = element.find_element(By.CSS_SELECTOR, "span.offers-reviews__description.offers-reviews__description_alter.offers-reviews__description_base.offers-reviews__description_condensed-additional.offers-reviews__description_multiline").text
#             sub_ans = [category, customs, title_review, mark_comment, pros, cons, review]
#             ans.append(sub_ans)
#         return ans

#     finally:
#         # Quit the WebDriver
#         driver.quit()


# async def crawl_data(links):
#     tasks = []
#     results = []

#     for link in tqdm(links):
#         link = link.strip()

#         # Create a task for each link
#         task = asyncio.create_task(process_link(link))
#         tasks.append(task)

#         # Limit the number of concurrent requests
#         if len(tasks) >= CONCURRENT_REQUESTS:
#             results.extend(await asyncio.gather(*tasks))
#             tasks = []

#     if tasks:
#         results.extend(await asyncio.gather(*tasks))

#     return results


# def save_to_excel(data):
#     headers = ['Категория', 'Товар', 'Заголовок отзыва',
#                'Комментарий к оценке', 'Достоинства',
#                'Недостатки', 'Отзыв']  # Add column names

#     df = pd.DataFrame(data, columns=headers)
#     df.to_excel(EXCEL_FILE, index=False)


# async def main():
#     with open(LINKS_FILE, "r", encoding="utf8") as f:
#         links = f.readlines()

#     links = [link.strip('\n') for link in links]

#     results = await crawl_data(links)
#     save_to_excel(results)


# if __name__ == "__main__":
#     asyncio.run(main())


import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import pandas as pd
from tqdm import tqdm
import warnings
import logging
import pickle
from selenium.webdriver.chrome.service import Service
from selenium.common.exceptions import NoSuchElementException, \
                                       ElementNotInteractableException, \
                                       StaleElementReferenceException, \
                                       ElementClickInterceptedException, \
                                       WebDriverException, \
                                       TimeoutException

# Disable logging messages from the specified logger
logging.getLogger("websockets").setLevel(logging.ERROR)

warnings.filterwarnings("ignore")

LINKS_FILE = "custom_links.txt"
EXCEL_FILE = "onliner_review_dataset_test_new.xlsx"
SERIALIZATION_INTERVAL = 100  # Number of links to process before serialization


def process_link(link, driver):
    # Load the page with JavaScript rendering
    driver.get(link)

    # Find the desired element by its CSS selector
    elements = driver.find_elements(By.CSS_SELECTOR, "div.offers-reviews__item")

    # Extract the desired information
    category = driver.find_elements(By.CSS_SELECTOR, 'li.breadcrumbs__item')[-2].text
    customs = driver.find_element(By.CSS_SELECTOR, 'h1.catalog-masthead__title.js-nav-header').text

    ans = []
    for element in tqdm(elements, leave=False):
        title_review = element.find_element(By.CSS_SELECTOR, "div.offers-reviews__description_font-weight_bold").text
        mark_comment = element.find_element(By.CSS_SELECTOR, "div.offers-reviews__grade-sign").text
        pros = element.find_element(By.CSS_SELECTOR, "div.offers-reviews__description_condensed-alter").text
        cons = element.find_elements(By.CSS_SELECTOR, "div.offers-reviews__description_max-width_xxxxll")[1].text
        review = element.find_element(By.CSS_SELECTOR, "span.offers-reviews__description_multiline").text
        sub_ans = [category, customs, title_review, mark_comment, pros, cons, review]
        ans.append(sub_ans)
    return ans


def crawl_data(links):
    # Set the options for the Chrome WebDriver
    service = Service()
    chrome_options = Options()
    chrome_options.add_argument('--headless')  # Run Chrome in headless mode (without opening a browser window)

    # Set up the Chrome WebDriver
    driver = webdriver.Chrome(options=chrome_options, service=service)
    driver.maximize_window()  # Maximizes the browser window

    results = []

    for i, link in enumerate(tqdm(links[22101:])):
        try:
            link = link.strip()

            result = process_link(link, driver)
            results.extend(result)

            # Serialize data every SERIALIZATION_INTERVAL links
            if (i + 1) % SERIALIZATION_INTERVAL == 0:
                serialize_data(results, f"serialized_data_15_{i + 1}.pickle")
        except (NoSuchElementException, ElementNotInteractableException, ElementClickInterceptedException, StaleElementReferenceException, WebDriverException, TimeoutException, IndexError):
            driver.refresh()

    # Quit the WebDriver
    driver.quit()

    return results


def serialize_data(data, filename):
    with open(filename, "wb") as file:
        pickle.dump(data, file)


def save_to_excel(data):
    headers = ['Категория', 'Товар', 'Заголовок отзыва',
               'Комментарий к оценке', 'Достоинства',
               'Недостатки', 'Отзыв']  # Add column names

    df = pd.DataFrame(data, columns=headers)
    df.to_excel(EXCEL_FILE, index=False)


def main():
    with open(LINKS_FILE, "r", encoding="utf8") as f:
        links = f.readlines()

    links = [link.strip('\n') for link in links]

    results = crawl_data(links)
    save_to_excel(results)


if __name__ == "__main__":
    main()
